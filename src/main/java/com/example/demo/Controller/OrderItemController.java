package com.example.demo.Controller;

import com.example.demo.Entity.Order;
import com.example.demo.Entity.OrderItem;
import com.example.demo.Entity.Shop;
import com.example.demo.OrderReturn;
import com.example.demo.Service.OrderItemService;
import com.example.demo.Service.OrderService;
import com.example.demo.Service.ShopService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
public class OrderItemController {
    private final OrderItemService orderItemService;

    public OrderItemController(OrderItemService orderItemService) {
        this.orderItemService = orderItemService;
    }

    @RequestMapping(value="/orderItem",method= RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(orderItemService.getAll());
    }
    @RequestMapping(value="/orderItem/{id}", method=RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> get(@PathVariable long id){
        return ResponseEntity.ok(orderItemService.getById(id));}
    @RequestMapping(value = "/orderItem/{id}", method = RequestMethod.DELETE, headers = "Accept=application/json")
    public void delete(@PathVariable int id){
        orderItemService.delete(id);
    }
    @RequestMapping(value="/orderItemm",method=RequestMethod.POST, headers = "Accept=application/json")
    public ResponseEntity<?> update(@RequestBody OrderItem orderItem){
        return ResponseEntity.ok(orderItemService.update(orderItem));
    }
    @PostMapping(value="/orderItem/create")
    public void save(@RequestHeader("shop_id") long shop_id, @RequestHeader("product_id") ArrayList<Integer> product_id, @RequestHeader("quantity") ArrayList<Integer> quantity, @RequestHeader("price") ArrayList<Float> price){
        orderItemService.save( shop_id, product_id, quantity, price);
    }
    @RequestMapping(value="/orderItem/auth", method=RequestMethod.GET, headers = "Accept=application/json")
    public ResponseEntity<?> getorderitem(@RequestHeader("shop_id") long shop_id){
        return ResponseEntity.ok(orderItemService.getByShopID(shop_id));}
    @RequestMapping(value="/orderItem/dashboard", method=RequestMethod.GET, headers = "Accept=application/json")
    public List<OrderReturn> getDashboard(){
        return orderItemService.getDashboard();}
}
