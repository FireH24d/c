package com.example.demo.Repository;


import com.example.demo.Entity.Shop;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShopRepository extends CrudRepository<Shop, Long> {
    Shop findById(long customerId);


}
