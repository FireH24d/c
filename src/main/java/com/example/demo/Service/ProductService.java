package com.example.demo.Service;

import com.example.demo.Entity.Factory;
import com.example.demo.Entity.Product;
import com.example.demo.Repository.FactoryRepository;
import com.example.demo.Repository.ProductRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getAll(){
        return (List<Product>) productRepository.findAll();
    }

    public ResponseEntity<?> getById(long id) {
        return ResponseEntity.ok(productRepository.findById(id));
    }

    public void delete(long id){
        productRepository.deleteById(id);
    }
    public Product update(@RequestBody Product product){
        return  productRepository.save(product);
    }
   public  List<Product> findAllByCategory_id(long id){
       return  productRepository.findAllByCategory_id(id);
    }
}
